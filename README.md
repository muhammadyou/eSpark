## eSpark Website

### Important Note

1. Be cautious while copying any json file from dev to VM environment
2. Libs for Angular ui-calendar for events calendar and tomtom.js for map needs to be manually copied to node_modules folder
3. Changes made to backend code requires restarting of the processes running by pm2. Refer to log commands
4. To uninstall devDependencies run : npm prune --production
5. To install dependencies only, ignoring the devdependencies, run: npm install --production
6. ensure the injected libraries in app.js and index.html cdns are same as in the master branch 



### App Setup in VM

1. Log in to VM 
2. Navigate to the dir
3. git clone -b dev <url>  
4. npm install 
5. navigate to node_modules -> pm2 -> bin -> pm2
6. chmod +x pm2
7. PORT=443 node_modules/pm2/bin/pm2 start bin/www   


### Database Setup in VM

1. Log in to VM 
2. sudo -i  // switch to root
3. vi <get the path from mongodb documentation>  // configure the package manager file
4. copy the configuration from mongodb doc to the file
5. Run: yum install -y mongodb-org  
6. service mongod start
7. ps -ef | grep mongod
8. In the app for db config, write the db VM IP and the port on which it's listening for mongo

#### Souce: https://www.youtube.com/watch?v=yc7B7GYw9LM


### Log Commands

#### Restart the VM services:
1. node_modules/pm2/bin/pm2 restart all

#### Delete the service running:
1. node_modules/pm2/bin/pm2 delete bin/www

#### View Live Logs
1. node_modules/pm2/bin/pm2 logs

#### View log file
1. cd /home/admin/.pm2/logs
2. vi www-out-0.log


